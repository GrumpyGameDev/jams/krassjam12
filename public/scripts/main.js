class App{
    static main(){
        let content = "";
        content+="<h1 class=\"title\">The Wyrms Splorr!!</h1>";
        content+="<div class=\"buttons\">";
        content+="<button class=\"button\" onclick=\"App.start()\"><span class=\"icon\"><i class=\"fas fa-play\"></i></span><span>Start</span></button>";
        content+="<button class=\"button\" onclick=\"App.help()\"><span class=\"icon\"><i class=\"fas fa-question\"></i></span><span>How to Play</span></button>";
        content+="<button class=\"button\" onclick=\"App.about()\"><span class=\"icon\"><i class=\"fas fa-info\"></i></span><span>About</span></button>";
        content+="</div>";
        Renderer.render(content);
    }
    static start(){
        Character.roll();
        CharacterCreation.show();
    }
    static help(){
        let content="";
        content+="<h1 class=\"title\">How to Play The Wyrms Splorr!!</h1>";
        content+="<div class=\"content\">";
        content+="The buttons describe what they do. You push the buttons, and the game does what the button says. Duh.";
        content+="</div>"
        content+="<div class=\"buttons\">";
        content+="<button class=\"button\" onclick=\"App.main()\"><span class=\"icon\"><i class=\"fas fa-arrow-left\"></i></span><span>Back</span></button>";
        content+="</div>";
        Renderer.render(content);
    }
    static about(){
        let content="";
        content+="<h1 class=\"title\">About The Wyrms Splorr!!</h1>";
        content+="<div class=\"content\">";
        content+="A game by TheGrumpyGameDev.";
        content+="</div>"
        content+="<div class=\"buttons\">";
        content+="<button class=\"button\" onclick=\"App.main()\"><span class=\"icon\"><i class=\"fas fa-arrow-left\"></i></span><span>Back</span></button>";
        content+="</div>";
        Renderer.render(content);
    }
}
