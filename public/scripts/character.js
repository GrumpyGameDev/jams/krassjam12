const STAT_MIND = "mind";
const STAT_MOVES = "moves";
const STAT_MOXIE = "moxie";
const STAT_MUSCLES = "muscles";
const STAT_MAGIC = "magic";
const STAT_EXTRA = "extra";
const STATS=[STAT_MIND,STAT_MOVES,STAT_MOXIE,STAT_MUSCLES,STAT_MAGIC,STAT_EXTRA];
const STAT_DEFAULTS=[1,1,1,1,0,1];
const STAT_REROLLS=[false,false,false,false,true,true];
const STAT_DICE = 7;
const STAT_DIE_SIZE = 6;
let _character=null;
class Character{
    static roll(){
        World.roll();
        let position = World.findPosition(MAP_OUTSET,TERRAIN_HOUSE);
        let character = {
            stats:{},
            position:{
                map:MAP_OUTSET,
                column:position.column,
                row:position.row
            },
            messages:[]
        };
        _character = character;
        for(let index in STATS){
            character.stats[STATS[index]]=0;
        }
        for(let index=0;index<STAT_DICE;++index){
            let die = Utility.rollDie(STAT_DIE_SIZE);
            character.stats[STATS[die]]++;
        }
        let rerolls = 0;
        for(let index in STAT_REROLLS){
            if(STAT_REROLLS[index]){
                rerolls+=character.stats[STATS[index]];
                character.stats[STATS[index]]=0;
            }
        }
        for(let index=0;index<rerolls;++index){
            let die = Math.floor(Math.random()*STAT_DIE_SIZE);
            character.stats[STATS[die]]++;
        }
        for(let index in STATS){
            character.stats[STATS[index]]+=STAT_DEFAULTS[index];
        }
        Quest.giveQuest(QUEST_MAIN,Character.addMessage);
    }
    static get character(){
        return _character;
    }
    static addMessage(message){
        _character.messages.push(message);
    }
    static clearMessages(){
        _character.messages=[];
    }
}