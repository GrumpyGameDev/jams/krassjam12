class CharacterCreation{
    static show(){
        let content="";
        let hasExtra = Character.character.stats[STAT_EXTRA]>0;
        content+="<h1 class=\"title\">Character Creation</h1>";
        content+="<table class=\"table is-bordered\">";
        for(var stat in Character.character.stats){
            content+="<tr>";
            content+="<td>"+stat+"</td>";
            content+="<td>"+Character.character.stats[stat];
            if(hasExtra && stat!=STAT_EXTRA){
                content+="<button class=\"button\" onclick=\"CharacterCreation.increase('"+stat+"')\"><span clas=\"icon\"><i class=\"fas fa-plus\"></i></span></button>";
            }
            content+="</td>";
            content+="</tr>"
        }
        content+="</table>";
        if(!hasExtra){
            content+="<button class=\"button\" onclick=\"Play.showState()\">Done!</button>"
        }
        Renderer.render(content);
    }
    static increase(stat){
        Character.character.stats[stat]++;
        Character.character.stats[STAT_EXTRA]--;
        CharacterCreation.show();
    }
}