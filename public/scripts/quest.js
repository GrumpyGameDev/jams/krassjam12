const QUEST_MAIN = "main";
const QUESTSTATE_VOID = "void";
const QUESTSTATE_STARTED = "started";
const QUESTSTATE_FINISHED = "finished";
const QUESTSTATE_CYCLOPSFOUND="cyclops-found";
const QUESTSTATE_ACCEPTEDTUTELAGE="accepted-tutelage";
let _quests = null;
class Quest{
    static init(){
        if(_quests==null){
            _quests={};
        }
    }
    static setQuestState(questId, questState, messageCallback){
        Quest.init();
        _quests[questId] = questState;
        switch(questId){
            case QUEST_MAIN:
                switch(questState){
                    case QUESTSTATE_STARTED:
                        messageCallback("You have a mission: find the Cyclops!");
                        break;
                }                
            break;
        }
    }
    static giveQuest(questId, messageCallback){
        Quest.init();
        if(Quest.getQuestState(questId)==QUESTSTATE_VOID){
            Quest.setQuestState(questId,QUESTSTATE_STARTED, messageCallback);
        }
    }
    static getQuestState(questId){
        Quest.init();
        return (_quests[questId] || QUESTSTATE_VOID);
    }
    static getActiveQuests(){
        let result = [];
        for(let quest in _quests){
            switch(_quests[quest]){
                case QUESTSTATE_STARTED:
                    result.push(quest);
                    break;
                default:
                    //do nothing - it aint on the list
                    break;
            }
        }
        return result;
    }
    static getQuestName(questId){
        switch(questId){
            case QUEST_MAIN:
                return "Main Quest";
            default:
                return questId;
        }
    }
    static getQuestStateName(questId,questState){
        switch(questId){
            case QUEST_MAIN:
                switch(questState){
                    case QUESTSTATE_STARTED:
                        return "Find the Cyclops!";
                    default:
                        return questState;
                }
            default:
                return questState;
        }
    }
}