const ITEM_BREADLOAF = "breadloaf";
let _inventory=null;
class Inventory{
    static init(){
        if(_inventory==null){
            _inventory = {};
        }
    }
    static addItem(itemId,itemCount,messageCallback){
        Inventory.init();
        _inventory[itemId]=(_inventory[itemId] || 0) + (itemCount || 1);
        let message = "You added an item to your inventory.";
        if(itemId==ITEM_BREADLOAF){
            message="You pluck a fresh, steamy loaf from the breadtree!"
        }
        messageCallback(message);
    }
    static getItems(){
        Inventory.init();
        let result = [];
        for(let itemId in _inventory){
            if(_inventory[itemId]>0){
                result.push(itemId);
            }
        }
        return result;
    }
    static getItemName(itemId){
        switch(itemId){
            case ITEM_BREADLOAF:
                return "A loaf of bread";
            default:
                return itemId;
        }
    }
    static getItemCount(itemId){
        Inventory.init();
        return _inventory[itemId] || 0;
    }
}