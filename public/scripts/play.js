const DIRECTION_NORTH = 'north';
const DIRECTION_EAST = 'east';
const DIRECTION_SOUTH = 'south';
const DIRECTION_WEST = 'west';
class Play{
    static getNavigationButtons(column,row){
        let content = "";
        content+="<div class=\"buttons\">";
        if(row>0){
            content+="<button class=\"button\" onclick=\"Play.move('"+DIRECTION_NORTH+"')\">North</button>"
        }
        if(column<(MAP_COLUMNS-1)){
            content+="<button class=\"button\" onclick=\"Play.move('"+DIRECTION_EAST+"')\">East</button>"
        }
        if(row<(MAP_ROWS-1)){
            content+="<button class=\"button\" onclick=\"Play.move('"+DIRECTION_SOUTH+"')\">South</button>"
        }
        if(column>0){
            content+="<button class=\"button\" onclick=\"Play.move('"+DIRECTION_WEST+"')\">West</button>"
        }
        if(0 < Quest.getActiveQuests().length){
            content+="<button class=\"button\" onclick=\"Play.showQuests()\">Quests</button>";
        }
        content+="<button class=\"button\" onclick=\"Play.showInventory()\">Inventory</button>";
        content+="</div>";
        return content;
    }
    static showInventory(){

    }
    static showDefaultState(){
        let content = "";
        let character = Character.character;
        let map = World.getMap(character.position.map);
        let column = character.position.column;
        let row = character.position.row;
        let mapCell = map[column][row];
        //mesages
        content+="<div class=\"content\">";
        for(let index in Character.character.messages){
            let message = Character.character.messages[index];
            content+="<p><i>"+message+"</i></p>"
        }
        //area description
        content+="<p>"+World.getTerrainDescription(mapCell)+"</p>";
        //navigation
        if(row==0){
            content+="<p>You can go no further north.</p>";
        }
        if(row==(MAP_ROWS-1)){
            content+="<p>You can go no further south.</p>";
        }
        if(column==0){
            content+="<p>You can go no further west.</p>";
        }
        if(column==(MAP_COLUMNS-1)){
            content+="<p>You can go no further east.</p>";
        }
        content+="</div>";

        content+=Play.getNavigationButtons(column,row);

        Renderer.render(content);
    }
    static acceptTutelage(){
        Character.clearMessages();
        Quest.setQuestState(QUEST_MAIN, QUESTSTATE_ACCEPTEDTUTELAGE);
        Character.addMessage("'Excellent! Now go make me a sandwich!'");
        Play.showState();
    }
    static denyTutelage(){
        let content="";
        content += "<p>'You deny the tutelage of the might Cyclops!?!'</p>";
        content += "<p><i>The Cyclops eats you and awaits a more promising student.</i></p>";
        content += "<h1>The End.</h1>";
        Renderer.render(content);
    }
    static showCyclopsState(){
        let content="";
        let character = Character.character;
        let map = World.getMap(character.position.map);
        let column = character.position.column;
        let row = character.position.row;
        for(let index in Character.character.messages){
            let message = Character.character.messages[index];
            content+="<p><i>"+message+"</i></p>"
        }
        content+="<p>You are in the presence of the Cyclops!</p>";
        switch(Quest.getQuestState(QUEST_MAIN)){
            case QUESTSTATE_STARTED:
                Quest.setQuestState(QUEST_MAIN, QUESTSTATE_CYCLOPSFOUND);
                break;
        }
        switch(Quest.getQuestState(QUEST_MAIN)){
            case QUESTSTATE_CYCLOPSFOUND:
                content+="<p>Wouldst thou like to learn how to warp spacetime?";
                content+="<button class=\"button\" onclick=\"Play.acceptTutelage()\">Yes</button>";
                content+="<button class=\"button\" onclick=\"Play.denyTutelage()\">No</button>";
                content+="</p>";
                break;
            case QUESTSTATE_ACCEPTEDTUTELAGE:
                content+="<p>(the Cyclops awaits his sandwich, patiently)</p>";
                break;
        }
        content+=Play.getNavigationButtons(column,row);
        Renderer.render(content);
    }
    static showState(){
        let character = Character.character;
        let map = World.getMap(character.position.map);
        let column = character.position.column;
        let row = character.position.row;
        switch(map[column][row]){
            case TERRAIN_CYCLOPS:
                Play.showCyclopsState();
                break;
            default:
                Play.showDefaultState();
                break;
        }

    }
    static showQuests(){
        let content = "";
        content+="<h1>Quests:</h1>";
        let quests = Quest.getActiveQuests();
        content+="<table class=\"table\">";
        for(let index in quests){
            let questId = quests[index];
            let questState = Quest.getQuestState(questId);
            content+="<tr>";
            content+="<td>"+Quest.getQuestName(questId)+"</td>";
            content+="<td>"+Quest.getQuestStateName(questId, questState)+"</td>";
            content+="</tr>";
        }
        content+="</table>";
        content+="<button class=\"button\" onclick=\"Play.showState()\">Done</button>";
        Renderer.render(content);
    }
    static move(direction){
        Character.clearMessages();
        Character.addMessage("You move "+direction+".");
        switch(direction){
            case DIRECTION_NORTH:
                Character.character.position.row--;
                break;
            case DIRECTION_EAST:
                Character.character.position.column++;
                break;
            case DIRECTION_SOUTH:
                Character.character.position.row++;
                break;
            case DIRECTION_WEST:
                Character.character.position.column--;
                break;
        }
        Play.showState();
    }
    static pluckLoaf(){
        Character.clearMessages();
        Inventory.addItem(ITEM_BREADLOAF,1,Character.addMessage);
        Play.showState();
    }
    static showInventory(){
        let content = "";
        content+="<h1>Inventory:</h1>";
        content+="<table class=\"table\">";
        let items = Inventory.getItems();
        if(items.length>0){
            for(let index in items){
                let itemId = items[index];
                content+="<tr><td>"+Inventory.getItemName(itemId)+"</td><td>"+Inventory.getItemCount(itemId)+"</td></tr>";
            }
        }else{
            content+="<tr><td colspan=\"2\">You have nothing in your inventory, which makes you a poor hoarder.</td></tr>";
        }
        content+="</table>";
        content+="<button class=\"button\" onclick=\"Play.showState()\">Done</button>";
        Renderer.render(content);
    }
}
