const MAP_OUTSET = "outset";
const MAP_COLUMNS = 4;
const MAP_ROWS = 4;
const TERRAIN_EMPTY="empty";
const TERRAIN_HOUSE="HOUSE";
const TERRAIN_CYCLOPS="CYCLOPS";
const TERRAIN_BREADTREE="BREADTREE";
class World{
    static getTerrainDescription(terrain){
        switch(terrain){
            case TERRAIN_EMPTY:
                return "There is nothing much to see around you.";
            case TERRAIN_HOUSE:
                return "You are at your home.";
            case TERRAIN_CYCLOPS:
                return "You are in the lair of the Cyclops. It smells of farts and sandwiches.";
            case TERRAIN_BREADTREE:
                return "You are at the breadtree. You are in luck, for it is time for the breadharvest.<button class=\"button\" onclick=\"Play.pluckLoaf()\">Pluck Loaf</button>"
        }
    }
    static newMap(terrainTable){
        let map = [];
        for(let column=0;column<MAP_COLUMNS;++column){
            let mapColumn = [];
            map.push(mapColumn);
            for(let row=0;row<MAP_ROWS;++row){
                mapColumn.push(TERRAIN_EMPTY);
            }
        }
        for(let terrain in terrainTable){
            let counter = terrainTable[terrain];
            while(counter>0){
                let column=Utility.rollDie(MAP_COLUMNS);
                let row = Utility.rollDie(MAP_ROWS);
                if(map[column][row]==TERRAIN_EMPTY){
                    map[column][row]=terrain;
                    counter--;
                }
            }
        }
        return map;
    }
    static roll(){
        let maps = {};
        this._maps = maps;
        let outset = World.newMap({"HOUSE":1,"CYCLOPS":1,"BREADTREE":1});
        maps[MAP_OUTSET]=outset;
    }
    static getMap(mapId){
        return this._maps[mapId];
    }
    static findPosition(mapId,terrain){
        let map = this._maps[mapId];
        for(let column in map){
            let mapColumn = map[column];
            for(let row in mapColumn){
                if(mapColumn[row]==terrain){
                    return{
                        column:column,
                        row:row
                    };
                }
            }
        }
        return null;
    }
}